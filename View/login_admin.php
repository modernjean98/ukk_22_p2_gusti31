<?php
  if(isset($_POST["submit"]) and $_POST["submit"]=="login"){
    require_once("../Models/User.php");
    $user = new User();

    $username = $_POST['username'];
    $pass = $_POST['pass'];
    $role = $_POST['role_id'];

    if(empty($username) AND empty($pass)){
      header("Location:".BASE_URL."View/login_admin.php?m=2");
      exit();
    }else{
      $result = $user->login($username, $pass);
      $get = $result->fetch_object();
      session_start();
      if(isset($get)){
        $_SESSION["id_user"]=$get->id_user;
        $_SESSION["username"]=$get->username;
        $_SESSION["role"]=$get->role;
        header("Location:".BASE_URL."View/kamar/kamar.php");
        exit();
      }else{
        header("Location:".BASE_URL."View/login_admin.php?m=1");
        exit();
      }
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../Public/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../Public/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../Public/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../Public/custom/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../Public/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../Home/index.html"><b>Hotel</b>baru</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <!-- Notification -->
    <?php
      require 'layoutPartial/alert.php';
    ?>
    <!-- END NOTIFICATIONS -->
    <form action="" id="login_form" method="post">
    <!-- Role ID 1 = Admin -->
    <input type="hidden" id="role" name="role" value="1">
    <p id="lblHeader" class="login-box-msg">Masuk Sebagai Admin</p>

      <div class="form-group has-feedback">
        <input type="text" id="user_nickname" name="username" class="form-control" placeholder="Nickname">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="user_pass" name="pass" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <input type="hidden" name="submit" value="login">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="login_resepsionis.php" id="btnAkses" class="btn btn-block btn-primary btn-flat btn-sm"><i class="fa fa-user"></i> Masuk Sebagai Resepsionis</a>
    </div>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../Public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../Public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../Public/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script src="loginAdmin.js" type="text/javascript"></script>
</body>
</html>
