var table;

//letakan metode yang akan digunakan di dalam function init()
function init() {
    get_data()

}
// Untuk menampilkan seluruh dataTable
function get_data(){
    table = $('#tbl_list').dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        "ajax" : {
            url : '../../Controller/Pemesanan.php?action=get_data',
            type : "POST",
            dataType: "json",
            error: function (e){
                console.log(e.responseText);
            }
        },
        responsive: true
    }).DataTable();
}


init();