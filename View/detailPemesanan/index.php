<?php
  require_once("../../Config/connection.php");
  session_start();
  if(isset($_SESSION["role"]) == 2){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php
    require_once("../layoutPartial/head_template.php");
  ?>
</head>
  <body class="hold-transition skin-blue sidebar-mini">
   <!-- Head in Here -->
    
    <!-- ================================================== -->

    <!-- ============================================== -->
    <!-- Header In Here -->
    <?php
        require_once("../layoutPartial/nav_template.php");
    ?>
    <!-- =============================================== -->
    <!-- Side In Here -->
    <?php
        require_once("../layoutPartial/aside_template.php");
    ?>
    <!-- =============================================== -->
<!-- <div class="wrapper"> -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    <div class="box">
        <div class="box-header">
              <h3 class="box-title">Detail Pemesanan</h3>
        </div>
        <!-- /.box-header -->
            <div class="box-body" id="daftarKamar">
              <table id="tbl_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Pemesanan</th>
                  <th>Email</th>
                  <th>No HP</th>
                  <th>Nama Tamu</th>
                  <th>Tipe Kamar</th>
                  <th>Check IN</th>
                  <th>Check Out</th>
                  <th style="width: 7%;">Jumlah</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
        <!-- /.box-body -->
        <!-- Form Tambah -->

    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- </div> -->
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../Public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../Public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../Public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../Public/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../Public/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../Public/js/demo.js"></script>

<!-- DataTable -->
<script src="../../Public/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../Public/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="detailPemesanan.js"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
<?php
  }else{
    header("Location:".BASE_URL);
  }
?>