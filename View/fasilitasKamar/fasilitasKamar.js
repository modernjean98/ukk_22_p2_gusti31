var table;

function init() {
    showForm(false);
    get_data();

    $("#formTambah").on("submit", function (e) {
        saveOrEdit(e);
    });
    // $.post("../../Controller/fasilitasKamar.php?action=select_tipe", function(r){
    //     $("#tipe_kamar").html(r);
    //     $('#tipe_kamar').selectpicker('refresh');
    // });
}

// Clean Form
function cleanString() {
    $("#id_fasilitaskamar").val("");
    $("#tipe_kamar").val("");
    $("#fasilitas_kamar").val("");
}

// Untuk menampilkan Form
function showForm(flag) {
    cleanString();
    if (flag) {
        $("#daftarKamar").hide();
        $("#formTambah").show();
        $("#btnTambah").hide();
    } else {
        $("#daftarKamar").show();
        $("#formTambah").hide();
        $("#btnTambah").show();
    }
}

// Untuk membatalkan showForm
function closeForm() {
    cleanString();
    showForm(false);
}

// Untuk menampilkan seluruh dataTable
function get_data(){
    table = $('#tbl_list').dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        "ajax" : {
            url : '../../Controller/fasilitasKamar.php?action=get_data',
            type : "POST",
            dataType: "json",
            error: function (e){
                console.log(e.responseText);
            }
        },
        responsive: true
    }).DataTable();
}

function saveOrEdit(e) {
    e.preventDefault();
    var formData = new FormData($("#formTambah")[0]);

    if ($('#tipe_kamar').val() == '' || $('#fasilitas_kamar').val() == ''){
        swal("Perhatian", "Silahkan Isi Data Terlebih Dahulu", "warning");
    } else {
        $.ajax({
            url: "../../Controller/fasilitasKamar.php?action=saveOrEdit",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {
                swal("Selamat", "Data Berhasil Disimpan", "success");
                showForm(false);
                table.ajax.reload();
            }
        });
    }
    cleanString();
}

function show(id_fasilitaskamar){
    $.post("../../Controller/fasilitasKamar.php?action=show", {id_fasilitaskamar: id_fasilitaskamar}, function (data){
        data = JSON.parse(data);
        showForm(true);

        $("#id_fasilitaskamar").val(data.id_fasilitaskamar);
        $("#tipe_kamar").val(data.tipe_kamar);
        // $('#tipe_kamar').selectpicker('refresh');
        $("#fasilitas_kamar").val(data.fasilitas_kamar);
    })
}

function delete_data(id_fasilitaskamar){
    swal({
        title: "Konfirmasi Penghapusan Data",
        text: "Data Akan Dihapus Permanen!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
            if (willDelete) {
                $.post("../../Controller/fasilitasKamar.php?action=delete_data", {id_fasilitaskamar: id_fasilitaskamar},
                function (data) { })
                table.ajax.reload();
                swal("Data Telah Dihapus", {
                    icon: "success",
                });
            } else {
                swal("Penghapusan Batal");
            }
        });
}



init()