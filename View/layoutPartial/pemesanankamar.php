
<div class="container">
<div class="row">
<div class="col-md-12">
<br>
<div class="box-header with-border center">
    <h3 class="box-title">Form Pemesanan</h3>
</div>
<form method="POST" id="ticket-form" class="form-horizontal">
    <div class="box-body">
                <!-- Nama Pemesan -->
                <div class="form-group">
                  <label for="nm_pemesanan" class="col-sm-2 control-label">Nama Pemesan</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="nm_pemesanan" id="nm_pemesanan" placeholder="Nama Pemesan">
                  </div>
                </div>
                <!-- Email -->
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-5">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                  </div>
                </div>
                <!-- No HP -->
                <div class="form-group">
                  <label for="no_hp" class="col-sm-2 control-label">No Handphone</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No Handphone">
                  </div>
                </div>
                <!-- Nama Tamu -->
                <div class="form-group">
                  <label for="nm_tamu" class="col-sm-2 control-label">Nama Tamu</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="nm_tamu" id="nm_tamu" placeholder="Nama Tamu">
                  </div>
                </div>
                <!-- Tipe Kamar -->
                <div class="form-group col-sm-2">
                  <label>Tipe Kamar</label>
                  <select name="id_kamar" class="form-control">
                      <?php
                        while($row = $result->fetch_object()){
                      ?>
                      <option value="<?php echo $row->id_kamar ?>"><?php echo $row->tipe_kamar ?></option>
                      <?php
                        }
                      ?>
                  </select>
                </div>
                <!-- Check IN dan Check Out -->
                <div class="form-group col-sm-2">
                  <label>Check-In</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar col-sm-5"></i>
                    </div>
                    <input type="date" name="cek_in" class="form-control pull-left" id="check-in">
                  </div>
                    <label>Check-Out</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar col-sm-5"></i>
                    </div>
                    <input type="date" name="cek_out" class="form-control pull-right" id="check-out">
                  </div>
                </div>
                <!-- Jumlah Kamar -->
                <div class="form-group col-sm-5">
                  <label for="jml" class="col-sm-5 control-label">Jumlah Kamar</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="jml" id="jml" placeholder="Jumlah Kamar">
                  </div>
                </div>
                        
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="reset" class="btn btn-default">Cancel</button>
                  <input type="hidden" name="submit" value="reservasi">
                  <button type="submit" name="action-save" value="save" class="btn btn-info pull-right">Pesan Sekarang</button>
                </div>
                <!-- /.box-footer -->
    </div>
</form>
</div>
</div>
</div>