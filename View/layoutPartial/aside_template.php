<!-- Login Sebagai Role ID 1 = ADMIN -->
<?php
  if($_SESSION["role"]==1){
?>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../kamar/kamar.php">
            <i class="fa fa-bed"></i> <span>Data Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fasilitasUmum/fasilitasUmum.php">
            <i class="fa fa-bed"></i> <span>Fasilitas Umum</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fasilitasKamar/fasilitasKamar.php">
            <i class="fa fa-bed"></i> <span>Fasilitas Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../logOut.php">
            <i class="fa fa-sign-out"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<!-- Login Sebagai Role ID 2 = Resepsionis -->
<?php
    }else{
?>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../detailPemesanan/">
            <i class="fa fa-ticket"></i> <span>Detail Pemesanan</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../logOut.php">
            <i class="fa fa-sign-out"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php
  }
?>