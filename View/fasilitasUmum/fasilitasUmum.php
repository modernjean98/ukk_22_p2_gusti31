<?php
  require_once("../../Config/connection.php");
  session_start();
  if(isset($_SESSION["id_user"]) &&($_SESSION["role"] !='2')){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php
    require_once("../layoutPartial/head_template.php");
  ?>
</head>
  <body class="hold-transition skin-blue sidebar-mini">
   <!-- Head in Here -->
    
    <!-- ================================================== -->

    <!-- ============================================== -->
    <!-- Header In Here -->
    <?php
        require_once("../layoutPartial/nav_template.php");
    ?>
    <!-- =============================================== -->
    <!-- Side In Here -->
    <?php
        require_once("../layoutPartial/aside_template.php");
    ?>
    <!-- =============================================== -->
<!-- <div class="wrapper"> -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    <div class="box">
        <div class="box-header">
              <h3 class="box-title">Fasilitas Umum</h3>
              <button type="button" id="btnTambah" onclick="showForm(true)" class="btn btn-success pull-right">
              <i class="fa fa-plus"></i>Tambah
              </button>
        </div>
        <!-- /.box-header -->
            <div class="box-body" id="daftarKamar">
              <table id="tbl_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Fasilitas Kamar</th>
                  <th>Ket</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
        <!-- /.box-body -->
        <!-- Form Tambah -->
        <form id="formTambah" method="POST">
          <input type="hidden" name="id_fasilitas" id="id_fasilitas">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">TambahFasilitasUmum</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                
                <!-- Fasilitas Umum -->
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Fasilitas Umum</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama_fasilitas" name="nama_fasilitas" placeholder="">
                  </div>
                </div>
                <br>
                <p>
                <!-- Ket -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Keterangan</label>

                  <div class="col-sm-10">
                  <textarea for="textarea1" class="textarea1" placeholder="..." id="ket" name="ket"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    <!-- <input type="text" class="form-control" id="fasilitas_kamar" name="fasilitas_kamar" placeholder="fasilitas"> -->
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-danger" onclick="closeForm()">Batal</button>
                <button type="submit" class="btn btn-info" id="btnSimpan">Simpan</button>
              </div>
              <!-- /.box-footer -->
          </div>
        </form>
        <!-- End Form Tambah -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- </div> -->
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../Public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../Public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../Public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../Public/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../Public/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../Public/js/demo.js"></script>

<!-- DataTable -->
<script src="../../Public/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../Public/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="fasilitasUmum.js"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
<?php
  }else{
    header("Location:".BASE_URL);
  }
?>