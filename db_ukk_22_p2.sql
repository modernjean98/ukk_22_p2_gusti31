-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2022 at 06:02 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ukk_22_p2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb`
--

CREATE TABLE `tb` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `role` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb`
--

INSERT INTO `tb` (`id_user`, `username`, `pass`, `role`) VALUES
(1, 'Admin', '1234', 1),
(2, 'Resepsionis', '12345', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_fasilitaskamar`
--

CREATE TABLE `tb_fasilitaskamar` (
  `id_fasilitaskamar` int(11) NOT NULL,
  `tipe_kamar` varchar(50) NOT NULL,
  `fasilitas_kamar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_fasilitaskamar`
--

INSERT INTO `tb_fasilitaskamar` (`id_fasilitaskamar`, `tipe_kamar`, `fasilitas_kamar`) VALUES
(6, '1', 'Coffee maker'),
(11, '1', '9jumhugbuiymbonimhiuvbuimu,1213245'),
(12, '1', 'aaaaaaaaaaaaabbbbbbbbbb');

-- --------------------------------------------------------

--
-- Table structure for table `tb_fasilitasumum`
--

CREATE TABLE `tb_fasilitasumum` (
  `id_fasilitas` int(11) NOT NULL,
  `nama_fasilitas` varchar(100) NOT NULL,
  `ket` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_fasilitasumum`
--

INSERT INTO `tb_fasilitasumum` (`id_fasilitas`, `nama_fasilitas`, `ket`) VALUES
(3, 'Kolam Renang', 'berada di lantai 3'),
(4, 'Restaurant', 'Berada di Basement');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kamar`
--

CREATE TABLE `tb_kamar` (
  `id_kamar` int(11) NOT NULL,
  `tipe_kamar` varchar(50) NOT NULL,
  `jml` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kamar`
--

INSERT INTO `tb_kamar` (`id_kamar`, `tipe_kamar`, `jml`) VALUES
(1, 'Deluxe', 34),
(5, 'Superior', 33);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `nm_pemesanan` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `nm_tamu` varchar(150) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  `cek_in` date NOT NULL,
  `cek_out` date NOT NULL,
  `jml` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pemesanan`
--

INSERT INTO `tb_pemesanan` (`id_pemesanan`, `nm_pemesanan`, `email`, `no_hp`, `nm_tamu`, `id_kamar`, `cek_in`, `cek_out`, `jml`) VALUES
(1, 'Agus', 'agus@gmail.com', '08225577897', 'Aguss', 1, '2022-05-09', '2022-06-10', 1),
(27, 'Diaz', 'diaz@gmail.com', '083132144', 'diaz', 1, '2022-05-11', '2022-05-12', 2),
(28, 'FF', 'ff@gmail.com', '083457689261', 'ffsejati', 5, '2022-05-11', '2022-05-14', 1),
(29, 'testing', 'testting@gmail.com', '7777777777', 'tesrt', 5, '2022-05-11', '2020-05-11', 1),
(30, 'testing', 'testting@gmail.com', '7777777777', 'tesrt', 5, '2022-05-11', '2020-05-11', 1),
(31, 'yyyy', 'yyyy@gmail.com', '12345272811', 'YyY', 1, '2022-05-11', '2022-04-24', 1),
(32, 'qqq', 'qq@gmaail.com', '1234121', 'qq', 1, '2022-05-18', '2022-05-20', 1),
(33, 'ssssss', 'sss@gmail.com', '976754132', 'sss', 5, '2022-05-11', '2022-05-14', 1),
(34, 'ssssss', 'sss@gmail.com', '976754132', 'sss', 5, '2022-05-11', '2022-05-14', 1),
(35, 'tert', 'ter@gmail.com', '324351213', 'tesrt', 1, '2022-05-11', '2022-05-13', 2),
(36, 'dd', 'dd@gmail.com', '1232442', 'dd', 5, '2022-05-11', '2022-05-12', 2),
(37, 'tester', 'tester@gmail.com', '64782424286', 'tester', 5, '2022-05-11', '2022-05-12', 2),
(38, 'nabati', 'nabati@gmail.com', '53627235813', 'nabati', 1, '2022-05-12', '2022-05-14', 2),
(39, 'chocco', 'chocco@gmail.com', '34832834292', 'chocco', 1, '2022-05-12', '2022-05-14', 1),
(40, 'dio', 'dio12@gmail.com', '087961289372', 'dio', 5, '2022-05-11', '2022-05-19', 1),
(41, 'coco', 'coco222@gmail.com', '08972397311', 'coco12', 5, '2022-05-11', '2022-05-13', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb`
--
ALTER TABLE `tb`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_fasilitaskamar`
--
ALTER TABLE `tb_fasilitaskamar`
  ADD PRIMARY KEY (`id_fasilitaskamar`);

--
-- Indexes for table `tb_fasilitasumum`
--
ALTER TABLE `tb_fasilitasumum`
  ADD PRIMARY KEY (`id_fasilitas`);

--
-- Indexes for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD PRIMARY KEY (`id_kamar`);

--
-- Indexes for table `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb`
--
ALTER TABLE `tb`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_fasilitaskamar`
--
ALTER TABLE `tb_fasilitaskamar`
  MODIFY `id_fasilitaskamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_fasilitasumum`
--
ALTER TABLE `tb_fasilitasumum`
  MODIFY `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
