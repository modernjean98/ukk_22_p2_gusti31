<?php
  require_once("Config/connection.php");
  session_start();
  if(isset($_SESSION["nm_pemesanan"])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Top Navigation</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="Public/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="Public/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="Public/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="Public/custom/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="Public/custom/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">



  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
    <div class="row">
        <div class="col-xs-12">
          <div class="box" style="margin-top: 50px">
            <div class="box-header">
              <h3 class="box-title">Cek Bukti Reservasi</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Nama Pemesanan </th>
                  <th>Email </th>
                  <th>No HP </th>
                  <th>Nama Tamu </th>
                  <th>Cek In </th>
                  <th>Cek Out </th>
                  <th>Jumlah Kamar </th>
                </tr>
                <tr>
                  <td><?php echo $_SESSION["nm_pemesanan"]; ?></td>
                  <td><?php echo $_SESSION["email"];?></td>
                  <td><?php echo $_SESSION["no_hp"];?></td>
                  <td><?php echo $_SESSION["nm_tamu"];;?></td>
                  <td><?php echo $_SESSION["cek_in"];?></td>
                  <td><?php echo $_SESSION["cek_out"];?></td>
                  <td><?php echo $_SESSION["jml"]?></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>


    </div>
    <!-- /.container -->

 
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="Public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="Public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="Public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="Public/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="Public/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="Public/js/demo.js"></script>
<script>
  window.print();
</script>
</body>
</html>



<?php
session_destroy();  
}else{
    header("Location:".BASE_URL);
  }
?>