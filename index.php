
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hotel Baru</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="Public/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="Public/css/adminlte.min.css">

  <link rel="stylesheet" href="Public/css/style.css">
</head>
<body class="hold-transition">
<nav class="navbar navbar-expand-md navbar-dark bg-info shadow">
    <div class="container">
        <a class="navbar-brand h1" href="index.html">
            <img src="Public/images/logo.jpg" width="30" height="30" class="d-inline-block align-top img-circle" alt="Logo">
            Hotel Baru
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="detailkamar.php">Kamar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="View/kamar/pemesanan.php">Pesan Kamar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="View/login_admin.php">Login</a>
            </li>
            
        </ul>
        </div>
    </div>
</nav>

<div class="container-fluid p-0">
    <img src="Public/images/banner.jpg" class="img img-fluid w-100">
</div>

<div class="container content">
<?php
    require 'View/layoutPartial/fasilitaskamar.php';
?>  
</div>

<!-- jQuery -->
<script src="Public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="Public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="Public/js/adminlte.min.js"></script>
</body>
</html>
