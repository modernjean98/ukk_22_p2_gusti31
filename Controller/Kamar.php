<?php
    require_once("../Models/Kamar.php");

    $kamar = new Kamar();

    // Cek jika ada id_kamar,tipe_kamar,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_kamar = isset($_POST["id_kamar"]) ? cleanString($_POST["id_kamar"]): "";
    $tipe_kamar = isset($_POST["tipe_kamar"]) ? cleanString($_POST["tipe_kamar"]): "";
    $jml = isset($_POST["jml"]) ? cleanString($_POST["jml"]): "";

    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_kamar)){
                // Jika id_kamar tidak ada pada req, jalankan method insert
                $response = $kamar->insert($tipe_kamar, $jml);
            } else {
                // Jika id_kamar ada, jalankan method edit
                $response = $kamar->update($id_kamar, $tipe_kamar, $jml);
            }
        break;

        case 'get_data' :
            $response = $kamar->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->tipe_kamar,
                    "1"=>$row->jml,
                    "2"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_kamar.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm" onclick="delete_data('.$row->id_kamar.')" title="Delete Data"><i class="fa fa-trash"></i></button>'
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
        case 'show' :
            $response = $kamar->show($id_kamar);
            echo json_encode($response);
        break;
        case 'delete_data' :
            $response = $kamar->delete_data($id_kamar);
        break;
    }