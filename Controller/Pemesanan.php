<?php
    require_once("../Models/detailPemesanan.php");

    $detailpemesanan = new detailPemesanan();

    switch ($_GET["action"]){
        case 'get_data' :
            $response = $detailpemesanan->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->nm_pemesanan,
                    "1"=>$row->email,
                    "2"=>$row->no_hp,
                    "3"=>$row->nm_tamu,
                    "4"=>$row->id_kamar,
                    "5"=>$row->cek_in,
                    "6"=>$row->cek_out,
                    "7"=>$row->jml
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
    }
