<?php
    require_once("../Models/fasilitasKamar.php");

    $fasilitaskamar = new fasilitasKamar();

    // Cek jika ada id$id_fasilitaskamar,tipe_kamar,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_fasilitaskamar = isset($_POST["id_fasilitaskamar"]) ? cleanString($_POST["id_fasilitaskamar"]): "";
    $tipe_kamar = isset($_POST["tipe_kamar"]) ? cleanString($_POST["tipe_kamar"]): "";
    $fasilitas_kamar = isset($_POST["fasilitas_kamar"]) ? cleanString($_POST["fasilitas_kamar"]): "";

    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_fasilitaskamar)){
                // Jika id$id_fasilitaskamar tidak ada pada req, jalankan method insert
                $response = $fasilitaskamar->insert($tipe_kamar, $fasilitas_kamar);
            } else {
                // Jika id$id_fasilitaskamar ada, jalankan method edit
                $response = $fasilitaskamar->update($id_fasilitaskamar, $tipe_kamar, $fasilitas_kamar);
            }
        break;

        case 'get_data' :
            $response = $fasilitaskamar->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->tipe_kamar,
                    "1"=>$row->fasilitas_kamar,
                    "2"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_fasilitaskamar.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm" onclick="delete_data('.$row->id_fasilitaskamar.')" title="Delete Data"><i class="fa fa-trash"></i></button>'
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
        case 'show' :
            $response = $fasilitaskamar->show($id_fasilitaskamar);
            echo json_encode($response);
        break;
        case 'delete_data' :
            $response = $fasilitaskamar->delete_data($id_fasilitaskamar);
        break;
        // case 'select_tipe' :
        //     require_once("../Models/Kamar.php");
        //     $kamar = new Kamar();
        //     $response = $kamar->get_data();
        //     while($row=$response->fetch_object()){
        //         echo '<option value='.$row->id_kamar.'>'.$row->tipe_kamar.'</option>';
        //     }
        // break;
    }