<?php
    require_once("../Models/fasilitasUmum.php");

    $fasilitasumum = new fasilitasUmum();

    // Cek jika ada id_fasilitas,nama_fasilitas,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_fasilitas = isset($_POST["id_fasilitas"]) ? cleanString($_POST["id_fasilitas"]): "";
    $nama_fasilitas = isset($_POST["nama_fasilitas"]) ? cleanString($_POST["nama_fasilitas"]): "";
    $ket = isset($_POST["ket"]) ? cleanString($_POST["ket"]): "";

    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_fasilitas)){
                // Jika id_kamar tidak ada pada req, jalankan method insert
                $response = $fasilitasumum->insert($nama_fasilitas, $ket);
            } else {
                // Jika id_kamar ada, jalankan method edit
                $response = $fasilitasumum->update($id_fasilitas, $nama_fasilitas, $ket);
            }
        break;

        case 'get_data' :
            $response = $fasilitasumum->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->nama_fasilitas,
                    "1"=>$row->ket,
                    "2"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_fasilitas.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm" onclick="delete_data('.$row->id_fasilitas.')" title="Delete Data"><i class="fa fa-trash"></i></button>'
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
        case 'show' :
            $response = $fasilitasumum->show($id_fasilitas);
            echo json_encode($response);
        break;
        case 'delete_data' :
            $response = $fasilitasumum->delete_data($id_fasilitas);
        break;
    }