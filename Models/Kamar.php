<?php

    require_once("../Config/connection.php");


    Class Kamar
    {
        public function __construct()
        {}

        public function get_data()
        {
            $sql = "SELECT * FROM tb_kamar";
            return runQuery($sql);
        }

        public function insert($tipe_kamar, $jml)
        {
            $sql = "INSERT INTO tb_kamar (tipe_kamar, jml) VALUES ('$tipe_kamar', '$jml')";
            return runQuery($sql);
        }

        public function update($id_kamar, $tipe_kamar, $jml)
        {
            $sql = "UPDATE tb_kamar SET tipe_kamar='$tipe_kamar', jml='$jml'
                    WHERE id_kamar='$id_kamar'";
            return runQuery($sql);
        }

        public function show($id_kamar)
        {
            $sql = "SELECT * FROM tb_kamar WHERE id_kamar='$id_kamar'";
            return runQueryRow($sql);
        }

        public function delete_data($id_kamar)
        {
            $sql = "DELETE FROM tb_kamar WHERE id_kamar='$id_kamar'";
            return runQuery($sql);
        }

    }