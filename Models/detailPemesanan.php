<?php

    require "../Config/connection.php";

    Class detailPemesanan{
        public function __construct(){

        }
    
        public function get_data()
        {
            $sql = "SELECT tb_pemesanan.id_pemesanan, 
            tb_pemesanan.nm_pemesanan, 
            tb_pemesanan.email, 
            tb_pemesanan.no_hp, 
            tb_pemesanan.nm_tamu, 
            tb_pemesanan.id_kamar, 
            tb_pemesanan.cek_in, 
            tb_pemesanan.cek_out, 
            tb_pemesanan.jml, 
            tb_kamar.tipe_kamar 
            FROM tb_pemesanan INNER JOIN tb_kamar ON tb_pemesanan.id_kamar = tb_kamar.id_kamar;";
            return runQuery($sql);
        }
    }